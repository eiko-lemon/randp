<h1>randp</h1>
<h5>simple program to get random passwords.</h1>

<pre><code>Usage: randp [OPTIONS]
    -n LENGTH   length of output.
    -e EXPRESSION   filter chars by a regular expression.
    -r STRING   use a custom string as candidates.

    use these to append candidates, otherwise '-uld' will be used. (can be run multiple times).
    -c      characters (symbols).
    -u      uppercase.
    -l      lowercase.
    -d      digits (numbers).
    -s      space.+

	-p		equivalent to -culd</code></pre>
